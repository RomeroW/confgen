Easy Config Generator
======================

--------------------------------------------------------------

Easy Config Genarator створює конфігураційні файли, беручи за основу
файлики json та файли props (прописані змінні, які дістаються з json файликів)

Встановлення
--------------
В директорії проекту, до кого підключається Easy Config Genarator виконуємо:
```sh
git submodule add https://RomeroW@bitbucket.org/RomeroW/confgen.git confgen
```
Запускаємо скрипт: 
```sh
php confgen/confgen
```
Після запуску отримуємо конфігураційні фали в корені проекту:
* dirs.php - тут вказані всі шляхи до наших файлів середовищ (env) чи props файлів
* props.php - конфіги, які будуть згенеровані.
Налаштовуємо отримані файли потрібним нам чином. Далі запускаємо команду, яка була
видана в консолі(php confgen/confgen)

Клонування проекту з Easy Config Genarator
------------------------------------------
Після того як склонували проект, отримаємо порожню директорію confgen. Запускаємо 
дві команди, які все зроблять. Перше, ініціалізуємо локальний файл конфігурацій:
```sh
git submodule init
```
Друге, отримуємо всі дані з Easy Config Genarator
```sh
git submodule update
```
Тепер можемо приступати до генерації конфігів.

Composer
--------
Для того, щоб конфіги генерувалися разом з composer install або composer update,
добавляємо у composer.json наступні дані:
```json
    "autoload": {
        "psr-0": {
            "confgen\\Confgen": ""
        }
    },
    "scripts": {
        "pre-install-cmd": [
            "confgen\\Confgen::generateConfigs"
        ],
        "pre-update-cmd": [
            "confgen\\Confgen::generateConfigs"
        ]
    }
```
Тепер на події composer`a install і update отримуватимемо відповідні конфігурації
для нашого середовища. 

Приклад
--------
Наш файл master.json (файл обов’язковий, містить спільні конфіги для різних
середовищ).
```json
{
    "data": {
        "ex": {
            "primary": "some string data",
            "val": 10
        }
    }
}
```
І власне файл нашого середовища (для прикладу dev.json)
```json
{
    "db": {
        "host": "127.0.0.1",
        "name": "simple",
        "username": "root",
        "password": ""
    }
}
```
Тепер файл prop (main.prop.php), який послугує шаблоном для майбутніх створених конфігів
```php
<?php
return array(
    'db' => array(
        'host' => '{{db.host}}',
        'database' => '{{db.name}}',
        'username' => '{{db.username}}',
        'password' => '{{db.password}}',
    )
);
```
Після запуску команди (php confgen/confgen) отримуємо файли, в які вносимо
відповідні шляхи.

dirs.php
```php
<?php

return array(
    //target configs folder (all configs store here)
    'target' => 'protected/config/',

    //dev.json, prod.json etc located here
    'environments' => 'protected/config/env/',

    //master.json here
    'master' => 'protected/config/env/'
);
```

props.php
```php
<?php

return array(
    array(
        'source' => 'protected/config/props/main.prop.php',
        'target' => 'protected/config/'
    ),
);
```
Запускаємо ще раз команду
```sh
php confgen/confgen
```
Нас просять ввести середовище, не жмемся, вводимо (за дефолтом dev). 
> У папці environments повинен знаходитися відповідний файл - з 
> назвою, яку ми вводимо, та з розширенням json.

Після того як ввели дані, чекаємо мить, і отримуємо наш конфігураційний файл
(той лежатиме там, куди йому показував target з prod.php):
```php
<?php
return array(
    'db' => array(
        'host' => '127.0.0.1',
        'database' => 'simple',
        'username' => 'root',
        'password' => '',
    )
);
```

PROFIT!!!