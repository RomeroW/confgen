<?php
namespace confgen;

require_once("Helpers.php");
/**
 * Created by PhpStorm.
 * User: romero
 * Date: 23.04.14
 * Time: 20:50
 */

class Confgen
{
    public static function generateConfigs()
    {
        //base configs
        $dirs = dirname(__DIR__).'/dirs.php';
        $props = dirname(__DIR__).'/props.php';

        if(!file_exists($dirs) || !file_exists($props)) {
            copy(__DIR__.'/dirs.php', $dirs);
            copy(__DIR__.'/props.php', $props);

            $confgen = basename(__DIR__);
            $message = 'Templates for dirs.php and prop.php are created. Config this files and run ';
            $message .= "\"php $confgen/confgen\"\n";
            echo $message;

            echo "\nPress ENTER to continue...";
            $handle = fopen("php://stdin", "r");
            fgets($handle);

            return false;
        }

        $directory = require_once($dirs);
        $configProps = require_once($props);

        if(!is_array($directory) || count($directory) == 0
            || !is_array($configProps) || count($configProps) == 0) {

            $confgen = basename(__DIR__);
            $message = 'Templates for dirs.php and prop.php are created. Config this files and run ';
            $message .= "\"php $confgen/confgen\"\n";

            echo($message);
            return false;
        }

        //required files
        $required = array(
            'master' => $directory['master'] . 'master.json',
        );

        //check required files existence
        foreach ($required as $key => $file) {
            if (!file_exists($file)) {
                echo("Cannot find $file - this file required for correct work.\n");
                return false;
            }
        }

        if (!file_exists($directory['environments'] . 'env.lock')
            || !file_exists($directory['environments'] .
                trim(file_get_contents($directory['environments'] . 'env.lock')) . '.json')
        ) {

            while (true) {
                echo "\nType environment file [dev]: ";
                $handle = fopen("php://stdin", "r");
                $line = trim(fgets($handle));
                if ($line === '') {
                    $line = 'dev';
                }

                $envFile = $directory['environments'] . $line . '.json';

                if (!file_exists($envFile)) {
                    echo "This config file does not exist.";
                    continue;
                }
                break;
            }

            file_put_contents($directory['environments'] . 'env.lock', $line);
        } else {
            $line = trim(file_get_contents($directory['environments'] . 'env.lock'));
            $envFile = $directory['environments'] . $line . '.json';
        }

        $masterConf = json_decode(file_get_contents($required['master']), true);
        $envConf = json_decode(file_get_contents($envFile), true);

        //merge JSON configs
        $configs = Helpers::array_merge_recursive2($masterConf, $envConf);

        foreach ($configProps as $conf) {

            if (isset($conf['source'])) {
                $file = $conf['source'];
            } else {
                echo "No source set for ";
                print_r($conf);
                echo "\n";
                continue;
            }

            $finalFile = $directory['target'] . str_replace('.prop', '', basename($file));
            if (isset($conf['target'])) {
                $finalFile = $conf['target'];
                if (is_dir($conf['target'])) {
                    $finalFile = $conf['target'] . str_replace('.prop', '', basename($file));
                }
            }

            echo "Processing $file... ";
            $content = file_get_contents($file);

            preg_match_all('#\{{(.*?)\}}#', $content, $match);

            $patterns = array_map(function ($value) {
                return '/' . $value . '/';
            }, $match[0]);
            $replacements = Helpers::create_replacements($configs, $match[1]);

            $withReplace = preg_replace($patterns, $replacements, $content);
            file_put_contents($finalFile, $withReplace);

            echo "Done!\n";
        }

        echo "Configs successfully generated!\n";
    }
} 