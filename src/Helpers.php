<?php
/**
 * Created by PhpStorm.
 * User: romero
 * Date: 23.04.14
 * Time: 20:52
 */
namespace confgen;

class Helpers {

    /**
     * Merge two arrays with replacements by priority
     *
     * @param $paArray1
     * @param $paArray2 - higher priority array
     * @return array
     */
    static function array_merge_recursive2($paArray1, $paArray2)
    {
        if (!is_array($paArray1) or !is_array($paArray2)) {
            return $paArray2;
        }
        foreach ($paArray2 AS $sKey2 => $sValue2) {
            $paArray1[$sKey2] = self::array_merge_recursive2(@$paArray1[$sKey2], $sValue2);
        }
        return $paArray1;
    }

    /**
     * Get value from multidimensional array
     *
     * @param $array
     * @param $key
     * @return mixed
     */
    static function get_value_recursive($array, $key)
    {
        if (!is_array($array)) {
            return $array;
        }
        if (!is_array($key)) {
            return $array[$key];
        }
        $s_key = array_shift($key);
        return self::get_value_recursive($array[$s_key], $key);
    }

    /**
     * @param array $array
     * @param $match_with
     * @return array
     */
    static function create_replacements(array $array, $match_with)
    {
        $result = array();

        foreach ($match_with as $match) {
            $result[] = self::get_value_recursive($array, explode('.', $match));
        }

        return $result;
    }
} 