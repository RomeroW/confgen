<?php

/**
 * default directories
 */

return array(
    /* EXAMPLE (UNCOMMENT AND CHANGE)
    //target configs folder (all configs store here)
    'target' => 'protected/config/',

    //dev.json, prod.json etc located here
    'environments' => 'protected/config/env/',

    //master.json here
    'master' => 'protected/config/env/',
     */
);